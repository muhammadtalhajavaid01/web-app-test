import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  isAuthenticated: false,
  imagesList: [],
  selectedImageId: -1,
  deleteStatus: false
}

const store = new Vuex.Store({
  state,
  getters: {
    isAuthenticated: (state) => {
      return state.isAuthenticated
    },
    imagesList: (state) => {
      return state.imagesList
    },
    selectedImageId: (state) => {
      return state.selectedImageId
    },
    deleteStatus: (state) => {
      return state.deleteStatus
    }
  },
  actions: {
    setSelectedImageId (context, id) {
      context.commit('setSelectedImageId', id)
    },
    addImage (context, newImage) {
      context.commit('addImage', newImage)
    },
    deleteImage (context, imageId) {
      context.commit('deleteImage', imageId)
    },
    deleteStatus (context, status) {
      context.commit('deleteStatus', status)
    }
  },
  mutations: {
    deleteStatus (state, status) {
      state.deleteStatus = status
      window.localStorage.setItem('deleteStatus', state.deleteStatus)
    },
    setSelectedImageId (state, id) {
      state.selectedImageId = id
      window.localStorage.setItem('selectedImageId', state.selectedImageId)
    },
    addImage (state, newImage) {
      let id = 1
      if (state.imagesList.length > 0) {
        id = state.imagesList[state.imagesList.length - 1].id + 1
      }
      var date = new Date()
      var dt = date.toLocaleDateString()
      var time = date.toLocaleTimeString()
      state.imagesList.push({id: id, image: newImage, creationDate: {date: dt, time: time}})
      window.localStorage.setItem('imagesList', JSON.stringify(state.imagesList.slice()))
    },
    deleteImage (state, imageId) {
      var imagesListTemp = state.imagesList.slice()
      for (var index = 0; index < imagesListTemp.length; index += 1) {
        if (parseInt(imagesListTemp[index].id) === parseInt(imageId)) {
          imagesListTemp.splice(index, 1)
          break
        }
      }

      state.imagesList = imagesListTemp
      window.localStorage.setItem('imagesList', state.imagesList.slice())
    }
  }
})

export default store
