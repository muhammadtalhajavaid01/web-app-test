import Vue from 'vue'
import AppLayout from './components/ContainerLayout/Layout.vue'
import router from './router'
import store from './vuex/index.js'
import jQuery from 'jquery'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)

window.jQuery = window.$ = jQuery

const app = new Vue({
  router,
  ...AppLayout,
  store
})

export { app, router }
